<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
if (empty($_SESSION['loggedin'])) header('Location: login.php');
require_once "connect.php";
global $conn;

$errormsg = false;

//PROCESS ADD USER FORM
if (isset($_POST['submit'])){
    $username = $conn->real_escape_string($_POST["username"]);
    $password = $conn->real_escape_string($_POST["password"]);
    $password = sha1($password);
    //Check input fields
    if (empty($password) || empty($username)){
        $errormsg ='
        <script>
            document.querySelector("#error").innerText = "Töltsd ki mindkét mezőt a hozzáadáshoz!";
        </script>';
    } else {
        $sql = "INSERT INTO users (username, password)
                    VALUES ('{$username}', '{$password}')";

        if ($conn->query($sql) === TRUE) {
            echo "Hozzáadva: " . $username;
        } else {
            echo "Hiba: " . $sql . "<br>" . $conn->error;
        }
        ?>
        <script>
            document.getElementById('name').value = null;
            document.getElementById('psw').value = null;
        </script>
        <?php
        echo "<meta http-equiv='refresh' content='0'>";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Felhasználók</title>
    <style>
        table, th, td {
            border: 1px solid;
            padding: 0.4rem;
        }
        table {
            border-collapse: collapse;
        }
    </style>
</head>
<body id="content">

<h1>Üdvözöllek, <?= $_SESSION['username'] ?>!</h1>

<section id="logout-form">
    <form id="logout-action" action="logout.php" method="post" >
        <a href="javascript:{}" onclick="document.getElementById('logout-action').submit();">Kijelentkezés</a>
    </form>
</section>

<?php ob_start(); ?>
<section id="section-users">
    <h3>Felhasználók listája</h3>
    <table>
        <thead>
        <tr>
            <td>ID</td>
            <td>Felhasználó</td>
            <td>Jelszó</td>
        </tr>
        </thead>
        <tbody>
        <?php
        $sql = "SELECT * FROM users";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()){
            printf('
                <tr>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                </tr>
                ', $row['userid'], $row['username'],$row['password']);
        };
        ?>
        </tbody>
    </table>
</section>

<section id="section-adduser">
    <h2>Felhasználó hozzáadása</h2>
    <form id="adduser-action" action="<?=$_SERVER['PHP_SELF']?>" method="post" >
        <h4 id="error" style="color: red;"></h4>
        <label for="name">Felhasználónév: </label>
        <input type="text" id="name" name="username">
        <br>
        <label for="psw">Jelszó: </label>
        <input type="text" id="psw" name="password">
        <br>
        <input type="submit" name="submit" value="Hozzáadás">
    </form>
</section>

<?php
$ob_result = ob_get_clean();

if ($_SESSION['username'] === "admin")
    echo $ob_result;

if ($errormsg) echo $errormsg;
?>

</body>
</html>